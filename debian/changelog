crashmail (1.7-4) unstable; urgency=medium

  * QA upload.
  * debian/gbp.conf: Set default packaging branch.
  * debian/control: Bump debhelper compat to v13.
  * Bump Standards-Version to 4.6.2.
  * d/control: Fix Vcs-* fields to use Salsa GitLab.
  * debian/README.source: Dropped, useless.
  * debian/rules: Drop old dbgsym migration.

 -- Boyuan Yang <byang@debian.org>  Tue, 14 Feb 2023 10:28:58 -0500

crashmail (1.7-3) unstable; urgency=medium

  * QA Upload.
  * Fix FTCBFS (Thanks Helmut Grohne) (Closes: #978124)
    + cross.patch: Don't pass flags via CC.
    + Let dh_auto_build pass cross tools to make.

 -- Nilesh Patra <nilesh@debian.org>  Sat, 09 Jul 2022 15:10:28 +0000

crashmail (1.7-2) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group. (Closes: #976832) (see #982204)

 -- Adrian Bunk <bunk@debian.org>  Sun, 07 Feb 2021 14:14:20 +0200

crashmail (1.7-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix ftbfs with GCC-10. (Closes: #957107)

 -- Sudip Mukherjee <sudipm.mukherjee@gmail.com>  Wed, 02 Dec 2020 21:49:50 +0000

crashmail (1.7-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Debian Policy v4.1.5.
  * Add a debian/patches/03-MitigateErroneousFailure.patch file.
  * Add an 'override_dh_auto_test' stanza in the debian/rules file in order to
    run the upstream tests.
  * Set the 'Priority' from 'extra' back to 'optional' in debian/control.
  * Change DEB_BUILD_MAINT_OPTIONS in debian/rules from '-format' to '+all'.
  * Changes to debian/copyright:
    + Set the 'Format' URL to use the HTTPS protocol.
    + Update copyright information for the new upstream.

 -- Robert James Clay <jame@rocasa.us>  Wed, 18 Jul 2018 19:14:23 -0400

crashmail (1.6-1) unstable; urgency=medium

  * New upstream release.
  * Add a debian/TODO file.
  * Add 'doc/AreafixHelp.txt' to debian/crashmail.docs.
  * Update the debhelper compatibility level from '8' to '9'.
  * Correct the file path in the 'jamlib' stanza in debian/copyright.
  * Add 'doc/example.prefs' to debian/crashmail.docs. (Closes: #791488)
  * Change 'dh_strip' option in debian/rules to generate a dbgsym package.
  * Changes to debian/control:
      Declare compliance with Debian Policy v4.1.1.
      Remove the no longer needed 'crashmail-dbg' stanza.
      Set the Vcs-Git URL to use 'https' instead of 'git'.
      Set the Vcs-Browser URL to use 'https' instead of 'http'.
      Update the debhelper Build-Depends version to '>= 9.20151219'.
  * Replace 'debian/crashlistout.1' with the upstream version in the file
    debian/crashmail.manpages.

 -- Robert James Clay <jame@rocasa.us>  Mon, 09 Oct 2017 19:59:13 -0400

crashmail (1.5-1) unstable; urgency=low

  * New upstream release.
    - Multiple fixes for compile time build issues.
    - Update and reconcile the overall version of Crashmail II with its existing
      tools versioning.
  * Rewrite debian/copyright for machine-readable format.
  * Rewrite debian/rules for use for newer versions of debhelper.
  * Changes to debian/patches/:
    - Use 01-ExamplePrefs.patch to set expected Debian paths in example.prefs.
    - Rewrite 02-Makefile.patch to not strip binaries.
  * Changes to debian/control:
    - Update the Debhelper Build-Depends version to '>= 8'.
    - Add stanza for debugging symbols crashmail-dbg package.
    - Update Standards-Version to v3.9.5, no changes required.
    - Update home page and Vcs* entries for SourceForge project.
    - Remove Homepage entry in binary package stanza as it is not necessary.
  * Add debian/README.source file, detailing using quilt for source patching.
  * Update the Debhelper compatibility to '8' in debian/compat.
  * Add debian/crashlistout.1 for the missing man page.
  * Add file debian/watch for use by uscan.

 -- Robert James Clay <jame@rocasa.us>  Wed, 23 Apr 2014 07:53:14 -0400

crashmail (0.71-5) unstable; urgency=low

  * New Maintainer. (Closes: #693229)
  * Move installation of doc/AreafixHelp.txt to debian/install.
  * Set 'Packaged for Debian by' to jame@rocasa.us in debian/copyright.
  * Move installation of doc files from the old top level Makefile to the
    dh_installdocs command line by adding them to a debian/crashmail.docs file.
  * Add usage of the CPPFLAGS,CFLAGS,and LDFLAGS variables to necessary src/
    directory Makefile.linux files.
  * Changes to debian/control:
    - Add 'dpkg-dev (>= 1.16.1~)' to Build-Depends.
    - Update Standards-Version to v3.9.4, no changes required.
    - Replace the Suggested non-free lha package with the unar package.
  * Changes to debian/rules:
    - Add build-arch and build-indep targets.
    - Update CPPFLAGS, CFLAGS, and LDFLAGS using dpkg-buildflags.
    - Moved installation of man pages from the old top level Makefile to the
      dh_installman command line.
    - Moved installation of example files from the old top level Makefile to a
      dh_installexamples command line.
    - Moved installation of executables from the old top level Makefile to a
      dh_install command line and a debian/crashmail.install file.
    - Move the make build & clean lines from the old top level Makefile to their
      corresponding debian/rules build and clean targets.
    - Move the bin directory lines from the old top level Makefile.

 -- Robert James Clay <jame@rocasa.us>  Fri, 25 Jan 2013 21:04:40 -0500

crashmail (0.71-4) unstable; urgency=low

  * Fix build problems on kFreeBSD.
   (Closes: Bug#592481)

 -- Peter Krefting <peterk@debian.org>  Thu, 12 Aug 2010 07:45:00 +0100

crashmail (0.71-3) unstable; urgency=low

  * Fix errors on 64-bit architectures.
   (Closes: Bug#511223)

 -- Peter Krefting <peterk@debian.org>  Sat, 11 Jul 2009 20:45:00 +0100

crashmail (0.71-2) unstable; urgency=low

  * Added support for installing unstripped binaries.
   (Closes: Bug#436670)
  * Updated policy and debhelper usage.

 -- Peter Karlsson <peterk@debian.org>  Sun, 02 Sep 2007 20:45:00 +0100

crashmail (0.71-1) unstable; urgency=low

  * New upstream release.

 -- Peter Karlsson <peterk@debian.org>  Wed, 28 Jul 2004 19:20:00 +0100

crashmail (0.70-2) unstable; urgency=low

  * Added missing document "filter.txt" to the package.

 -- Peter Karlsson <peterk@debian.org>  Mon, 22 Dec 2003 21:00:00 +0100

crashmail (0.70-1) unstable; urgency=low

  * New upstream release.
  * Updated copyright file to match current license terms.

 -- Peter Karlsson <peterk@debian.org>  Sun, 14 Dec 2003 00:00:00 +0100

crashmail (0.62-1) unstable; urgency=low

  * New upstream release.
  * Updated to Standards-Version 3.5.5:
   - Move the AreafixHelp.txt to /usr/share/crashmail since it is
     referenced from the sample configuration.

 -- peter karlsson <peterk@debian.org>  Fri, 17 Aug 2001 22:45:00 +0200

crashmail (0.60-3) unstable; urgency=low

  * Bugfix for crashwrite: No longer cut lines longer than 100 characters.

 -- peter karlsson <peterk@debian.org>  Fri, 16 Mar 2001 22:45:00 +0100

crashmail (0.60-2) unstable; urgency=low

  * New maintainer (previously sponsored).
  * Updated to Standards-Version 3.0.1.
  * Added Build-Depends.
  * Suggests goldedplus (Fidonet reader).
  * Move sample preferences file to be compliant with debian-policy.

 -- peter karlsson <peterk@debian.org>  Fri, 25 Aug 2000 20:10:00 +0200

crashmail (0.60-1) unstable; urgency=low

  * New upstream release.
  * Changed sample preferences to reflect Debian GNU/Linux paths.
  * AreaFixHelp.txt no longer compressed, so crashmail can use it directly.
  * Suggests zip, unzip and lha for [de]compression of mail bundles.

 -- Per Lundberg <plundis@debian.org>  Sun, 21 Nov 1999 20:30:47 +0100

crashmail (0.52-1) unstable; urgency=low

  * Initial Release.

 -- Per Lundberg <plundis@debian.org>  Tue, 19 Oct 1999 20:54:38 +0200
